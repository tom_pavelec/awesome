# Awesome list

Selection of best tools.

## Table of Contents
- [PHP](#php)
- [Web development](#web-development)
    - [Style](#style)
    - [Tools](#tools)
- [Waiting for evaluation](#waiting-for-evaluation)

## Web development
*Web development - frameworks and libs*

### PHP

#### Libraries
* [Webmozart assert](https://github.com/webmozart/assert) - assert lib, usable in tests, value object validation, etc
* [Codeception](https://github.com/Codeception/Codeception) - testing framework
* [Safe PHP](https://github.com/thecodingmachine/safe) - less code, less error checking (simple stan)

#### Code analysis and testing
* [PHPStan](https://github.com/phpstan/phpstan) - PHP Static Analysis Tool
    * [PHPStan playground](https://phpstan.org/)

#### Security
* [Roave Security Advisories](https://github.com/Roave/SecurityAdvisories) - This package ensures that your application doesn't have installed dependencies with known security vulnerabilities.

### Style
*CSS frameworks and libs*
* [Animate.css](https://daneden.github.io/animate.css/) - CSS animations
* [Fontawesome](https://fontawesome.com/) - Icon font
* [Materialize.css](https://materializecss.com/) - CSS framework

### Tools
*Handy tools for web development*
* [Regex101](https://regex101.com/) - Regex tester
* [3v4l.org](https://3v4l.org/) - Run code in 200+ PHP versions simultaneously
* [Codepen](https://codepen.io/) - The best place to build, test, and discover front-end code.

## Waiting for evaluation
*interesting projects at first look, need to be evaluated*
* [PHP design patterns](https://github.com/domnikl/DesignPatternsPHP)
* [Functional PHP](https://github.com/lstrojny/functional-php) - Set of functional methods for PHP (+ partial application, currying)
* [PHP iter](https://github.com/nikic/iter) - Iteration primitives using generators
* [PHP router](https://github.com/nikic/FastRoute) - some inspiration
